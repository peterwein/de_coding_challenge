Data Engineering Coding Challenge

Below are the requirements necessary to run the repository code.

Language: Python 3.7.x

Modules: Flask 1.1.1

Instructions to run:

Download the repository

Run text_comparison.py without arguments for comparison of sample text

Run text_comparison.py with 3 arguments of any 3 text to compare

Run app.py for flask framework web app
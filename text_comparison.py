"""
Notes:
This will compare 3 different texts and return the jaccard score and cosine similarity.
It will also return the minimum and maximum for each based on the three texts.
"""
def main(text1=None, text2=None, text3=None):

    def dataClean(text):
        """
        clean the data (remove punctuation and stop words, lower case)
        """
        txt = text.lower().split( )
        txt = [word.strip('.').strip(',') for word in txt]
        stopwords = ['the', 'a', 'an', 'in', 'as', 'if', 'is', 'or', 'very', 'to', 'or', 'i']
        txt = set([word for word in txt if not word in stopwords])
        return txt

    def jaccardScore(t1, t2):
        """
        based on our clean text, determine the Jacccard Similarity
        (A&B)/(A + B + (A&B)) (which words are shared compared those that are not)
        """
        #determine Jaccard score
        jaccard_score = len(t1.intersection(t2)) / len(t1.union(t2))

        return jaccard_score

    def cosineSimiarity(t1, t2):
        """
        measure angle between two vectors, each representing the text 
        """
        #form a set containing keywords of both strings (similar to jaccard)
        vector1 = []
        vector2 = [] 
        rvector = t1.union(t2)

        #create vectors of 1/0 for union or intersection words
        for w in rvector: 
            if w in t1: 
                vector1.append(1)
            else: 
                vector1.append(0) 
            if w in t2: 
                vector2.append(1) 
            else: 
                vector2.append(0) 

        #cosine formula to calculate similarity
        c = 0
        for i in range(len(rvector)): 
                c+= vector1[i]*vector2[i] 
        cosine = c / float((sum(vector1)*sum(vector2))**0.5) 
        
        return cosine

    def minMax(results, scoreList):
        #determine max and min scores (and find the appropriate keys)
        maximum = {}
        minimum = {}
        for scoretype in scoreList:
            maximum[scoretype] = {}
            minimum[scoretype] = {}
            maximum[scoretype][list(results[scoretype].keys())[list(results[scoretype].values()).index(max([results[scoretype][x] for x in results[scoretype]]))]] = max([results[scoretype][x] for x in results[scoretype]])
            minimum[scoretype][list(results[scoretype].keys())[list(results[scoretype].values()).index(min([results[scoretype][x] for x in results[scoretype]]))]] = min([results[scoretype][x] for x in results[scoretype]])

        return maximum, minimum
   

    if text1 is None:
        text1 = r"The easiest way to earn points with Fetch Rewards is to just shop for the products you already love. If you have any participating brands on your receipt, you'll get points based on the cost of the products. You don't need to clip any coupons or scan individual barcodes. Just scan each grocery receipt after you shop and we'll find the savings for you."
    if text2 is None:
        text2 = r"The easiest way to earn points with Fetch Rewards is to just shop for the items you already buy. If you have any eligible brands on your receipt, you will get points based on the total cost of the products. You do not need to cut out any coupons or scan individual UPCs. Just scan your receipt after you check out and we will find the savings for you."
    if text3 is None:
        text3 = r"We are always looking for opportunities for you to earn more points, which is why we also give you a selection of Special Offers. These Special Offers are opportunities to earn bonus points on top of the regular points you earn every time you purchase a participating brand. No need to pre-select these offers, we'll give you the points whether or not you knew about the offer. We just think it is easier that way."

    t1 = dataClean(text1)
    t2 = dataClean(text2)
    t3 = dataClean(text3)

    #create results dict for storing metrics
    results = {}

    #store the results from the jaccard score
    results['jaccard_score'] = {}
    results['jaccard_score']['t1_t2'] = jaccardScore(t1, t2)
    results['jaccard_score']['t1_t3'] = jaccardScore(t1, t3)
    results['jaccard_score']['t2_t3'] = jaccardScore(t2, t3)

    #store the results from the cosine similarity
    results['cosine_score'] = {}
    results['cosine_score']['t1_t2'] = cosineSimiarity(t1, t2)
    results['cosine_score']['t1_t3'] = cosineSimiarity(t1, t3)
    results['cosine_score']['t2_t3'] = cosineSimiarity(t2, t3)

    #store minimum and max for any scores we calculate
    maximum, minimum = minMax(results, ['jaccard_score', 'cosine_score'])

    #abstract storage into data dict (will also include text, used for flask app)
    data = {}
    data['text'] = {
        'text 1': text1, 
        'text 2': text2, 
        'text 3': text3
    }
    data['results'] = results
    data['maximum'] = maximum
    data['minimum'] = minimum
    
    print(data)
    return data 

if __name__ == '__main__':
    main()
"""
Example of how to do the post request for this app:
import requests, json
headers = {'content-type': 'application/json'}
url = 'http://localhost:5000/add'
payload = {
    "text1":"This is a sample of the first text.", 
    "text2":"This is a sample of the second text. Let's ensure it is the most different.",
    "text3":"This is one of many samples of the third text."
    }
response = requests.post(url, json=payload, verify=False, headers=headers)

y = json.loads(response.text)
print(y)
"""
from flask import Flask, jsonify, request, abort
import text_comparison

app = Flask(__name__)


@app.route('/', methods=['GET'])
def get_metrics_all():
    data = text_comparison.main()
    return jsonify({'data': data})

@app.route('/add', methods=['POST'])
def run_text_comparison():
    if not request.json or not ('text1' and 'text2' and 'text3') in request.json:
        abort(400)
    text = {
        'text1': request.json['text1'],
        'text2': request.json['text2'],
        'text3': request.json['text3']
    }
    data = text_comparison.main(text['text1'], text['text2'], text['text3'])
    return jsonify({'data': data}), 201

if __name__ == '__main__':
    app.run(debug=True)